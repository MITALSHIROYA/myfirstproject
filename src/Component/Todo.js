import React, { useState } from 'react'
import to1 from '../img/1.png';
const Todo = () => {

  const [adddata, setAdddata]=useState("");
  const [item,setItem]=useState([]);
  const [togglesubmit,setTogglesubmit]=useState(true);
  const [isEdititem,setIsEditItem]=useState(null);

const addItem=()=>{
  if(!adddata){
      alert("plz fill data");
  }else if(adddata && !togglesubmit){
    setItem(
      item.map((elem)=>{
        if(elem.id === isEdititem){
          return { ...elem,name:adddata}
       
        } 
        return elem;  
      })
    )
    setTogglesubmit(true);
    setAdddata("  ");
    setIsEditItem(null);
  }
  else { 
const allinputAdata={id:new Date().getTime().toString(),name:adddata}
  setItem([... item,allinputAdata]);
  setAdddata("");
  }
 
}


const deletItem = (index)=>{ 
  const updataItem =item.filter((elem)=>{
    return(index!==elem.id) ;
  });
  setItem(updataItem);
}
const allItem=()=>{
  setItem([]);
}

const editItem=(id)=>{
  let newEditeItem=item.find((elem)=>{
    return elem.id === id
  });
  setTogglesubmit(false);
  setAdddata(newEditeItem.name);
  setIsEditItem(id);

}
  return (
 <>
 <div className='main_div'> 
<div>
  <figure>
    <img src={to1} alt="images" /><br></br><br></br>
    <figcaption>Add your list hear 📓 </figcaption>
  </figure>
    <div className='additem'>
      <input type="text"  placeholder='✍️Add item...'
      value={adddata} onChange={(e)=> setAdddata(e.target.value)}/>
      {

        togglesubmit? <button onClick={addItem}>➕</button>:<button onClick={addItem}>📃</button> 
      }
      
  
     
    </div>
    <div>
      {
        item.map((elem,ind)=>{
          return(
          <div className='inner' key={elem.id}>
          <h3>{elem.name}   <button onClick={()=>editItem(elem.id)}>📃</button> <button onClick={()=>deletItem(elem.id)}>🧧</button></h3>
         
        </div>
        );
        })
      }
    
    </div>
    <div >
      <button className='btn1' data-sm-link-text="remove all" onClick={allItem}>Remove All Item </button>
    </div>

</div>
</div>
 </>
  )
}

export default Todo;